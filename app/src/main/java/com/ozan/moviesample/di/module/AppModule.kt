package com.ozan.moviesample.di.module

import android.app.Application
import android.content.Context
import com.ozan.moviesample.di.scope.AppScope
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @AppScope
    @Provides
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }

}