package com.ozan.moviesample.di.component

import android.app.Application
import com.ozan.domain.di.component.DomainComponent
import com.ozan.moviesample.di.module.ActivityBuilderModule
import com.ozan.moviesample.di.module.AppModule
import com.ozan.moviesample.di.module.ViewModelModule
import com.ozan.moviesample.di.scope.AppScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

@AppScope
@Component(
        modules = [
            AndroidInjectionModule::class,
            AppModule::class,
            ActivityBuilderModule::class,
            ViewModelModule::class
        ],
        dependencies = [
            DomainComponent::class
        ]
)
interface AppComponent :AndroidInjector<DaggerApplication>{

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun setDomainComponent(domainComponent: DomainComponent):Builder

        fun build(): AppComponent
    }

    override fun inject(instance: DaggerApplication?)
}