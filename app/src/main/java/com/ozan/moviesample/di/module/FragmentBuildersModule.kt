package com.ozan.moviesample.di.module

import com.ozan.moviesample.ui.MovieDetailFragment
import com.ozan.moviesample.ui.MovieListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun movieListFragment(): MovieListFragment

    @ContributesAndroidInjector
    internal abstract fun movieDetailFragment(): MovieDetailFragment

}
