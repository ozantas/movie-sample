package com.ozan.moviesample.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ozan.moviesample.di.scope.AppScope
import com.ozan.moviesample.vm.MovieDetailVM
import com.ozan.moviesample.vm.MovieListVM
import com.ozan.moviesample.vm.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @AppScope
    @Binds
    @IntoMap
    @ViewModelKey(MovieListVM::class)
    internal abstract fun bindMovieListVM(viewModel: MovieListVM): ViewModel

    @AppScope
    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailVM::class)
    internal abstract fun bindMovieDetailVM(viewModel: MovieDetailVM): ViewModel

    @AppScope
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}