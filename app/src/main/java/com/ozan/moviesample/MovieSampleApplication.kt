package com.ozan.moviesample

import com.ozan.domain.di.component.DaggerDomainComponent
import com.ozan.domain.di.component.DomainComponent
import com.ozan.moviesample.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class MovieSampleApplication: DaggerApplication(){

    private val domainComponent: DomainComponent by lazy {
        DaggerDomainComponent
            .builder()
            .build()
    }

    override fun onCreate() {
        super.onCreate()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent
            .builder()
            .application(this)
            .setDomainComponent(domainComponent)
            .build()
    }

}