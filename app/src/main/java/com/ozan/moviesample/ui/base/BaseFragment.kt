package com.ozan.moviesample.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.ozan.moviesample.BR
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class BaseFragment<VM : ViewModel, DB : ViewDataBinding> : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel:VM
    protected lateinit var binding: DB
    abstract val getLayoutId: Int
    abstract val viewModelClass: Class<VM>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutId, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.setVariable(BR.viewModel,viewModel)
        return binding.root
    }

    protected open fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModelClass)
    }

    fun loadFragment(containerId: Int, fragment: Fragment, fragmentManager: FragmentManager?, addToBackStack: Boolean) {
        val ft = fragmentManager?.beginTransaction()
        if (addToBackStack) {
            ft?.addToBackStack(fragment.javaClass.name)
        }
        ft?.replace(containerId, fragment)
        ft?.commit()
    }

}