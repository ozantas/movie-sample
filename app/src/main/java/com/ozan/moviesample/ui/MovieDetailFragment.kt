package com.ozan.moviesample.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ozan.domain.uimodel.MovieUiModel

import com.ozan.moviesample.R
import com.ozan.moviesample.databinding.FragMovieDetailBinding
import com.ozan.moviesample.ui.base.BaseFragment
import com.ozan.moviesample.vm.MovieDetailVM

class MovieDetailFragment : BaseFragment<MovieDetailVM,FragMovieDetailBinding>() {

    companion object{
        private const val KEY_SELECTED_MOVIE="key.selected.movie"

        fun newInstance(selectedMovie: MovieUiModel): MovieDetailFragment {
            val fragment=MovieDetailFragment()
            val bundle= Bundle()
            bundle.putParcelable(KEY_SELECTED_MOVIE,selectedMovie)
            fragment.arguments= bundle
            return fragment
        }
    }

    override val getLayoutId: Int = R.layout.frag_movie_detail
    override val viewModelClass: Class<MovieDetailVM> = MovieDetailVM::class.java

    private val selectedMovie:MovieUiModel? by lazy {
        arguments?.getParcelable<MovieUiModel>(KEY_SELECTED_MOVIE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        viewModel.showMovieDetail(selectedMovie)
        return binding.root
    }

}