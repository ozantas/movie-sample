package com.ozan.moviesample.ui.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.ozan.moviesample.R

@BindingAdapter("imageUrl")
fun bindImageUrl(imageView: ImageView,url:String?){
    url?.let {
        Glide.with(imageView.context)
            .load(it)
            .placeholder(R.drawable.ic_image)
            .into(imageView)
    }
}