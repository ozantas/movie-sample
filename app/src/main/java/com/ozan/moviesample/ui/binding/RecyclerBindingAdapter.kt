package com.ozan.moviesample.ui.binding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ozan.moviesample.ui.base.VerticalSpaceItemDecoration

@BindingAdapter(value = ["verticalItemSpace"])
fun bindVerticalSpace(recyclerView: RecyclerView,space:Int){
    val verticalSpaceDivider = VerticalSpaceItemDecoration(space)
    recyclerView.addItemDecoration(verticalSpaceDivider)
}