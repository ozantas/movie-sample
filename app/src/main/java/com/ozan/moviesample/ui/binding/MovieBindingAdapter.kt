package com.ozan.moviesample.ui.binding

import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.ozan.moviesample.R

@BindingAdapter(value = ["ratingTextStyle"])
fun bindRatingTextStyle(textView: TextView,rating:Double?){
    if (rating==null) return
    when(rating){
        in 0.0..7.99-> textView.setTextColor(ContextCompat.getColor(textView.context,R.color.gray600))
        else -> textView.setTextColor(ContextCompat.getColor(textView.context,R.color.teal))
    }
    textView.text = rating.toString()
}