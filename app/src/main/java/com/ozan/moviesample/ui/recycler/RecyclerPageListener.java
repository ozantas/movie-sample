package com.ozan.moviesample.ui.recycler;

public interface RecyclerPageListener {
        void onPageEnded(int position);
}