package com.ozan.moviesample.ui.recycler.movie

import androidx.recyclerview.widget.DiffUtil
import com.ozan.domain.uimodel.MovieUiModel

class MovieDiffUtil constructor(
    private var oldList:List<MovieUiModel>,
    private var newList:List<MovieUiModel>
): DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return newList[newItemPosition].name == oldList[oldItemPosition].name
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return newList[newItemPosition] == oldList[oldItemPosition]
    }

}