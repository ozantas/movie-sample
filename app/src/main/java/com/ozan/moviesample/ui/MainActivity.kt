package com.ozan.moviesample.ui

import android.os.Bundle
import com.ozan.moviesample.R
import com.ozan.moviesample.databinding.ActivityMainBinding
import com.ozan.moviesample.ui.base.BaseActivity

class MainActivity : BaseActivity<ActivityMainBinding>() {

    override val layoutRes: Int=R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadFragment(R.id.frmMain,MovieListFragment.newInstance(),false)
    }
}