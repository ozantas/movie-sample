package com.ozan.moviesample.ui.recycler.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ozan.domain.uimodel.MovieUiModel
import com.ozan.moviesample.R
import com.ozan.moviesample.databinding.ItemMovieBinding
import com.ozan.moviesample.ui.recycler.RecyclerPageListener

open class MovieAdapter(
    private val itemClickListener: MovieItemClickListener,
    private val recyclerPageListener: RecyclerPageListener
): RecyclerView.Adapter<MovieViewHolder>() {

    private var movieList:List<MovieUiModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemMovieBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_movie,parent,false)
        val holder= MovieViewHolder(binding)
        holder.itemBinding.root.setOnClickListener {
            itemClickListener.onClick(movieList[holder.adapterPosition],holder.adapterPosition)
        }
        return holder
    }

    override fun getItemCount(): Int = movieList.size

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bindData(movieList[position])
        if (position == movieList.size - 1) {
            recyclerPageListener.onPageEnded(position)
        }
    }

    fun show(newList:List<MovieUiModel>){
        val diffCallback = MovieDiffUtil(movieList, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        diffResult.dispatchUpdatesTo(this)
        movieList= newList
    }

}