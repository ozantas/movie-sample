package com.ozan.moviesample.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.ozan.domain.uimodel.MovieUiModel
import com.ozan.moviesample.R
import com.ozan.moviesample.databinding.FragMovieListBinding
import com.ozan.moviesample.ui.base.BaseFragment
import com.ozan.moviesample.ui.recycler.RecyclerPageListener
import com.ozan.moviesample.ui.recycler.movie.MovieAdapter
import com.ozan.moviesample.ui.recycler.movie.MovieItemClickListener
import com.ozan.moviesample.vm.MovieListVM

class MovieListFragment : BaseFragment<MovieListVM, FragMovieListBinding>() {

    companion object{
        fun newInstance(): MovieListFragment {
            return MovieListFragment()
        }
    }

    override val getLayoutId: Int=R.layout.frag_movie_list
    override val viewModelClass: Class<MovieListVM> = MovieListVM::class.java

    private val itemClickListener= object : MovieItemClickListener {
        override fun onClick(item: MovieUiModel, position: Int) {
            loadFragment(R.id.frmMain,MovieDetailFragment.newInstance(item),fragmentManager,true)
        }
    }

    private val recyclerPageListener= RecyclerPageListener { viewModel.updateMovies() }

    private val movieAdapter: MovieAdapter by lazy {
        MovieAdapter(itemClickListener,recyclerPageListener)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.rvMovie.adapter= movieAdapter
        viewModel.updateMovies()
        subscribeMovieList()
        subscribeMovieLoadError()
        return binding.root
    }

    private fun subscribeMovieList() {
        viewModel.movieList.observe(this, Observer {
            movieAdapter.show(it)
        })
    }

    private fun subscribeMovieLoadError(){
        viewModel.movieLoadError.observe(this, Observer { errorMessage ->
            Toast.makeText(requireContext(),errorMessage,Toast.LENGTH_SHORT).show()
        })
    }

}