package com.ozan.moviesample.ui.recycler.movie

import androidx.recyclerview.widget.RecyclerView
import com.ozan.domain.uimodel.MovieUiModel
import com.ozan.moviesample.databinding.ItemMovieBinding

class MovieViewHolder(
    val itemBinding: ItemMovieBinding
): RecyclerView.ViewHolder(itemBinding.root) {

    fun bindData(movieUiModel: MovieUiModel) {
        itemBinding.movie= movieUiModel
    }
}