package com.ozan.moviesample.ui.recycler.movie

import com.ozan.domain.uimodel.MovieUiModel

interface MovieItemClickListener {
    fun onClick(item:MovieUiModel,position:Int)
}