package com.ozan.moviesample.vm

import androidx.lifecycle.MutableLiveData
import com.ozan.domain.uimodel.MovieUiModel
import javax.inject.Inject

class MovieDetailVM @Inject constructor(

):BaseViewModel() {

    val selectedMovie= MutableLiveData<MovieUiModel>()

    fun showMovieDetail(mMovie: MovieUiModel?) {
        mMovie.let {
            selectedMovie.postValue(it)
        }
    }

}