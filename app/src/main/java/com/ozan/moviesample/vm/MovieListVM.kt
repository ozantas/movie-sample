package com.ozan.moviesample.vm

import androidx.lifecycle.MutableLiveData
import com.ozan.common.DataHolder
import com.ozan.domain.uimodel.MovieUiModel
import com.ozan.domain.usecase.GetMovieListUseCase
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MovieListVM @Inject constructor(
    private val getMovieListUseCase: GetMovieListUseCase
):BaseViewModel() {

    val movieList= MutableLiveData<List<MovieUiModel>>()
    val movieLoadError= MutableLiveData<String>()
    val loadingState= MutableLiveData<Boolean>()

    fun updateMovies(){
        getMovieListUseCase.getMovieList()
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {
                if(getMovieListUseCase.pageNumber<2){
                    loadingState.postValue(true)
                }
            }
            .doFinally { loadingState.postValue(false) }
            .subscribe{
                when(it){
                    is DataHolder.Success -> movieList.postValue(it.data)
                    is DataHolder.Error -> movieLoadError.postValue(it.error.message)
                }
            }
            .addTo(disposableList)
    }

}