package com.ozan.data.extension

import com.ozan.common.DataHolder
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

fun<T : Any> Observable<T>.addToDataHolder(): Observable<DataHolder<T>> {
    return this
        .observeOn(Schedulers.io())
        .subscribeOn(Schedulers.io())
        .map<DataHolder<T>> { data-> DataHolder.Success(data)}
        .onErrorResumeNext { throwable: Throwable ->
            Observable.just(
                DataHolder.Error(throwable)
            )
        }
        .doOnError{ t: Throwable ->  t.printStackTrace()}
}