package com.ozan.data.repository

import com.ozan.common.DataHolder
import com.ozan.data.remote.model.MovieResponse
import io.reactivex.Observable

interface MovieRepository {

    fun getMovieList(pageNumber:Int): Observable<DataHolder<MovieResponse>>
}