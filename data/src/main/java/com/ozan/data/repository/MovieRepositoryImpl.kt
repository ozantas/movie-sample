package com.ozan.data.repository

import com.ozan.common.DataHolder
import com.ozan.data.extension.addToDataHolder
import com.ozan.data.remote.api.MovieApi
import com.ozan.data.remote.model.MovieResponse
import io.reactivex.Observable
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val movieApi: MovieApi
):MovieRepository {

    override fun getMovieList(pageNumber:Int): Observable<DataHolder<MovieResponse>> {
        return movieApi.getMovieResponse(pageNumber).addToDataHolder()
    }

}