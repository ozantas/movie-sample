package com.ozan.data.di.module

import com.ozan.data.repository.MovieRepository
import com.ozan.data.repository.MovieRepositoryImpl
import dagger.Binds
import dagger.Module

@Module(includes = [NetworkModule::class])
abstract class RepositoryModule {

    @Binds
    internal abstract fun bindMovieRepository(movieRepository: MovieRepositoryImpl):MovieRepository
}