package com.ozan.data.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class ApiKeyQualifier
