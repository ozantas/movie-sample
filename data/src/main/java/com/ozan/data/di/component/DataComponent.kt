package com.ozan.data.di.component

import com.ozan.data.di.module.RepositoryModule
import com.ozan.data.repository.MovieRepository
import dagger.Subcomponent

@Subcomponent(
    modules = [
        RepositoryModule::class
    ]
)
interface DataComponent {

    @Subcomponent.Builder
    interface Builder {
        fun build(): DataComponent
    }
}