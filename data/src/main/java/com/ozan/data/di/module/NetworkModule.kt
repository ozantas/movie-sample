package com.ozan.data.di.module

import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.ozan.data.di.qualifier.ApiKeyQualifier
import com.ozan.data.di.qualifier.BaseApiUrlQualifier
import com.ozan.data.di.qualifier.BaseImageUrlQualifier
import com.ozan.data.remote.api.MovieApi
import com.ozan.data.remote.interceptor.MovieApiInterceptor
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {

    @Provides
    @BaseApiUrlQualifier
    fun provideBaseUrl(): String = "https://api.themoviedb.org"

    @Provides
    @BaseImageUrlQualifier
    fun provideBaseImageUrl(): String = "https://image.tmdb.org/t/p/"

    @Provides
    @ApiKeyQualifier
    fun provideApiKey(): String = "f3b915111c4d28c966efc8a087989eb0"

    @Provides
    fun provideGson(): Gson = Gson()

    @Provides
    fun provideMovieApi(okHttpClient: OkHttpClient, gson: Gson,@BaseApiUrlQualifier baseUrl:String): MovieApi {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(MovieApi::class.java)
    }

    @Provides
    fun provideOkHttp(loggingInterceptor:HttpLoggingInterceptor,movieApiInterceptor: MovieApiInterceptor): OkHttpClient{
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(movieApiInterceptor)
            .readTimeout(45, TimeUnit.SECONDS)
            .connectTimeout(45, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return loggingInterceptor
    }

    @Provides
    fun provideMovieApiInterceptor(@ApiKeyQualifier apiKey:String): MovieApiInterceptor {
        return MovieApiInterceptor(apiKey)
    }

}