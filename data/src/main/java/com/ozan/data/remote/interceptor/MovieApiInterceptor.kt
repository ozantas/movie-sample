package com.ozan.data.remote.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class MovieApiInterceptor(
    private val apiKey:String
):Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val url = originalRequest.url.newBuilder()
            .addQueryParameter("api_key", apiKey)
            .build()

        val requestBuilder = originalRequest.newBuilder()
            .url(url)

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}