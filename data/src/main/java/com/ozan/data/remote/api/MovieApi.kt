package com.ozan.data.remote.api

import com.ozan.data.remote.model.MovieResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApi {

    @GET("3/tv/popular")
    fun getMovieResponse(@Query("page") pageNumber:Int): Observable<MovieResponse>
}