package com.ozan.domain.usecase

import com.ozan.common.DataHolder
import com.ozan.data.repository.MovieRepository
import com.ozan.domain.mapper.MovieModelMapper
import com.ozan.domain.uimodel.MovieUiModel
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class GetMovieListUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
    private val movieModelMapper: MovieModelMapper
) {

    var pageNumber=0
    private var oldData:Observable<List<MovieUiModel>> = Observable.just(listOf())

    fun getMovieList(): Observable<DataHolder<List<MovieUiModel>>>{
        pageNumber++
        return Observable.zip(
            oldData,
            movieRepository.getMovieList(pageNumber),
            BiFunction { mOldData, newData ->
                when(newData){
                    is DataHolder.Success -> {
                        val newUiModel= movieModelMapper.mapToUiModel(newData.data)
                        val updatedData:List<MovieUiModel> = mOldData.plus(newUiModel)
                        oldData= Observable.just(updatedData)
                        return@BiFunction DataHolder.Success(updatedData)
                    }
                    is DataHolder.Error -> return@BiFunction DataHolder.Error(newData.error)
                }
            })
    }
}