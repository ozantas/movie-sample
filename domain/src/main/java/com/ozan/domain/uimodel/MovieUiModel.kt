package com.ozan.domain.uimodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieUiModel (
    val name: String,
    val smallPictureUrl:String,
    val largePictureUrl:String,
    val originalPictureUrl:String,
    val rating:Double,
    val overview:String,
    val popularity:String,
    val firstAirDate:String
):Parcelable