package com.ozan.domain.di.module

import com.ozan.domain.di.scope.DomainScope
import com.ozan.data.di.module.RepositoryModule
import com.ozan.data.repository.MovieRepository
import com.ozan.domain.mapper.MovieModelMapper
import com.ozan.domain.usecase.GetMovieListUseCase
import dagger.Module
import dagger.Provides

@Module(includes = [RepositoryModule::class,MapperModule::class])
class UseCaseModule {

    @DomainScope
    @Provides
    fun provideGetMovieListUseCase(movieRepository: MovieRepository,mapper: MovieModelMapper): GetMovieListUseCase {
        return GetMovieListUseCase(movieRepository,mapper)
    }

}