package com.ozan.domain.di.component

import com.ozan.data.di.component.DataComponent
import com.ozan.domain.di.module.UseCaseModule
import com.ozan.domain.di.scope.DomainScope
import com.ozan.domain.usecase.GetMovieListUseCase
import dagger.Component

@DomainScope
@Component(
    modules = [
        UseCaseModule::class
    ]
)
interface DomainComponent {

    fun dataComponentBuilder(): DataComponent.Builder

    fun getMovieListUseCase():GetMovieListUseCase

    @Component.Builder
    interface Builder {
        fun build(): DomainComponent
    }

}