package com.ozan.domain.di.module

import com.ozan.data.di.qualifier.BaseImageUrlQualifier
import com.ozan.domain.di.scope.DomainScope
import com.ozan.domain.mapper.MovieModelMapper
import dagger.Module
import dagger.Provides

@Module
class MapperModule {

    @DomainScope
    @Provides
    fun provideMovieModelMapper(@BaseImageUrlQualifier imageBaseUrl:String): MovieModelMapper {
        return MovieModelMapper(imageBaseUrl)
    }

}