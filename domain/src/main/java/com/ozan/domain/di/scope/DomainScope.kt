package com.ozan.domain.di.scope

import javax.inject.Scope

@Scope
@Retention
annotation class DomainScope