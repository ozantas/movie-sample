package com.ozan.domain.mapper

import com.ozan.data.di.qualifier.BaseImageUrlQualifier
import com.ozan.data.remote.model.MovieResponse
import com.ozan.domain.uimodel.MovieUiModel
import javax.inject.Inject

class MovieModelMapper @Inject constructor(
    @BaseImageUrlQualifier private val baseImageUrl:String
){

    private val smallWidth="w92"
    private val largeWidth="w780"
    private val originalWidth="original"

    fun mapToUiModel(movieResponse: MovieResponse): List<MovieUiModel> {
        val uiModelList= mutableListOf<MovieUiModel>()

        movieResponse.results?.forEach { movieModel ->
            val uiModel= MovieUiModel(
                name = movieModel?.name?: "",
                smallPictureUrl = baseImageUrl+smallWidth+movieModel?.posterPath,
                largePictureUrl = baseImageUrl+largeWidth+movieModel?.posterPath,
                originalPictureUrl = baseImageUrl+originalWidth+movieModel?.posterPath,
                rating = movieModel?.voteAverage?: 0.0,
                overview = movieModel?.overview?:"",
                popularity = movieModel?.popularity.toString(),
                firstAirDate = movieModel?.firstAirDate ?:""
            )
            uiModelList.add(uiModel)
        }

        return uiModelList
    }
}