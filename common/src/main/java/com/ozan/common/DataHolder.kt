package com.ozan.common

sealed class DataHolder<out T:Any> {

    data class Success<out T:Any>(val data:T): DataHolder<T>()
    data class Error(val error: Throwable): DataHolder<Nothing>()

}